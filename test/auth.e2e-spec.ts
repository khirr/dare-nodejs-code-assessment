import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import { AppModule } from '@/app.module';
import * as request from 'supertest';

describe('AuthController (e2e)', () => {
  let app: INestApplication;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  it('/api/v1/login (POST): Bad Request', () => {
    return request(app.getHttpServer()).post('/api/v1/login').expect(400);
  });

  it('/api/v1/login (POST): Not Authorized', () => {
    const payload = { client_id: 'fake', client_secret: 'fake' };
    return request(app.getHttpServer())
      .post('/api/v1/login')
      .send(payload)
      .expect(401);
  });

  it('/api/v1/login (POST): Success', () => {
    const payload = { client_id: 'dare', client_secret: 's3cr3t' };
    return request(app.getHttpServer())
      .post('/api/v1/login')
      .send(payload)
      .expect(200);
  });
});
