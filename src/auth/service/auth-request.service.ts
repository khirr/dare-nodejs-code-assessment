import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import axios from 'axios';

@Injectable()
export class AuthRequestService {
  private axios;
  private sessionToken: string;

  constructor(private readonly configService: ConfigService) {
    this.axios = axios.create();
    this.setupInterceptor();
  }

  private setupInterceptor() {
    this.axios.interceptors.response.use(null, async (error) => {
      if (error.config && error.response && error.response.status === 401) {
        await this.updateSessionToken();
        error.config.headers['Authorization'] = `Bearer ${this.sessionToken}`;
        return axios.request(error.config);
      }
      return Promise.reject(error);
    });
  }

  private async updateSessionToken(): Promise<void> {
    const loginUrl = `${this.configService.get<string>('api.baseUrl')}/login`;
    this.sessionToken = await axios
      .post(loginUrl, {
        client_id: this.configService.get<string>('api.clientId'),
        client_secret: this.configService.get<string>('api.clientSecret'),
      })
      .then((result) => result.data['token']);
  }

  async get(url: string, data: any): Promise<any> {
    return this.axios({
      method: 'get',
      url,
      data,
      headers: {
        Authorization: `Bearer ${this.sessionToken}`,
      },
    }).then((result) => result.data);
  }
}
