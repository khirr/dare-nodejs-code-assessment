import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { JwtService } from '@nestjs/jwt';
import { ILoginResult } from '../interface/login-result.interface';

@Injectable()
export class AuthService {
  constructor(
    private readonly configService: ConfigService,
    private readonly jwtService: JwtService,
  ) {}

  validateUser(clientId: string, clientSecret): boolean {
    return (
      clientId === this.configService.get<string>('api.clientId') &&
      clientSecret === this.configService.get<string>('api.clientSecret')
    );
  }

  login(clientId: string): ILoginResult {
    const payload = { clientId };
    return {
      access_token: this.jwtService.sign(payload),
      type: 'Bearer',
      expires_in: parseInt(this.configService.get<string>('jwt.ttl')),
    };
  }
}
