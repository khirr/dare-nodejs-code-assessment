import {
  BadRequestException,
  Body,
  Controller, HttpCode,
  Post,
  UnauthorizedException
} from "@nestjs/common";
import { LoginInput } from '@/auth/dto/login.input';
import { ILoginResult } from '@/auth/interface/login-result.interface';
import { AuthService } from '@/auth/service/auth.service';

@Controller()
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Post('/api/v1/login')
  @HttpCode(200)
  async login(@Body() loginInput: LoginInput): Promise<ILoginResult> {
    if (!loginInput.client_id || !loginInput.client_secret) {
      throw new BadRequestException(
        'Required params: client_id, client_secret',
      );
    }
    if (
      this.authService.validateUser(
        loginInput.client_id,
        loginInput.client_secret,
      )
    ) {
      return this.authService.login(loginInput.client_id);
    } else {
      throw new UnauthorizedException();
    }
  }
}
