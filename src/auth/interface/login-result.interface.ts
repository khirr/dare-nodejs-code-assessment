export interface ILoginResult {
  access_token: string;
  type: string;
  expires_in: number;
}
