import {
  BadRequestException,
  Body,
  Controller,
  Get,
  HttpCode,
  Param,
  UseGuards,
} from '@nestjs/common';
import { JwtAuthGuard } from '@/auth/jwt-auth.guard';
import { PolicyService } from '@/policy/policy.service';
import { PoliciesInput } from '@/policy/dto/policies.input';
import { IPolicy } from '@/policy/interface/policy.interface';

@Controller()
@UseGuards(JwtAuthGuard)
export class PolicyController {
  constructor(private readonly policyService: PolicyService) {}

  @Get('/api/v1/policies')
  @HttpCode(200)
  async policies(@Body() input: PoliciesInput): Promise<IPolicy[]> {
    return this.policyService.getPolicies(input.limit);
  }

  @Get('/api/v1/policies/:id')
  @HttpCode(200)
  async policy(@Param('id') id: string) {
    if (!id) {
      throw new BadRequestException('Required params: id');
    }
    return this.policyService.getPolicy(id);
  }
}
