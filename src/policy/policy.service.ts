import { AuthRequestService } from '@/auth/service/auth-request.service';
import { Injectable, NotFoundException } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { IPolicy } from './interface/policy.interface';
import { IPolicyFromServer } from '@/policy/interface/policy-from-server.interface';

@Injectable()
export class PolicyService {
  constructor(
    private readonly configService: ConfigService,
    private readonly authRequestService: AuthRequestService,
  ) {}

  async findPoliciesFromServer(): Promise<IPolicyFromServer[]> {
    const url = `${this.configService.get<string>('api.baseUrl')}/policies`;
    return (await this.authRequestService.get(url, {})) as IPolicyFromServer[];
  }

  private parseServerPolicyToPolicy(serverPolicy: IPolicyFromServer): IPolicy {
    return {
      id: serverPolicy.id,
      amountInsured: serverPolicy.amountInsured,
      email: serverPolicy.email,
      inceptionDate: serverPolicy.inceptionDate,
      installmentPayment: serverPolicy.installmentPayment,
    };
  }

  async getPolicies(limit = 10): Promise<IPolicy[]> {
    const result = await this.findPoliciesFromServer();
    return result
      .slice(0, limit)
      .map((item) => this.parseServerPolicyToPolicy(item));
  }

  async getPolicy(id: string): Promise<IPolicy> {
    const policies = await this.findPoliciesFromServer();
    const policy = policies.find((policy) => policy.id === id);
    if (!policy) {
      throw new NotFoundException();
    }
    return this.parseServerPolicyToPolicy(policy);
  }

  getPoliciesByClientId(
    policiesFromServer: IPolicyFromServer[],
    clientId: string,
  ): IPolicy[] {
    const policies = policiesFromServer.filter(
      (policy) => policy.clientId === clientId,
    );
    return policies.map((policy) => this.parseServerPolicyToPolicy(policy));
  }
}
