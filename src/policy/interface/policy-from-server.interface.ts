export interface IPolicyFromServer {
  id: string;
  amountInsured: string;
  email: string;
  inceptionDate: string;
  installmentPayment: boolean;
  clientId: string;
}
