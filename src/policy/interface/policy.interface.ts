export interface IPolicy {
  id: string;
  amountInsured: string;
  email: string;
  inceptionDate: string;
  installmentPayment: boolean;
}
