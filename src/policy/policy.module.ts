import { AuthModule } from '@/auth/auth.module';
import { Module } from '@nestjs/common';
import { PolicyController } from './policy.controller';
import { PolicyService } from './policy.service';

@Module({
  controllers: [PolicyController],
  providers: [PolicyService],
  imports: [AuthModule],
  exports: [PolicyService],
})
export class PolicyModule {}
