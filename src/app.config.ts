export default () => ({
  api: {
    baseUrl: process.env.API_BASE_URL,
    clientId: process.env.API_CLIENT_ID,
    clientSecret: process.env.API_CLIENT_SECRET,
  },
  jwt: {
    secretKey: process.env.JWT_SECRET_KEY,
    ttl: process.env.JWT_TTL,
  },
});
