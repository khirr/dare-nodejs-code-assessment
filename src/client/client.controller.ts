import {
  BadRequestException,
  Body,
  Controller,
  Get,
  HttpCode,
  Param,
  UseGuards,
} from '@nestjs/common';
import { JwtAuthGuard } from '@/auth/jwt-auth.guard';
import { IPolicy } from '@/policy/interface/policy.interface';
import { ClientsInput } from '@/client/dto/clients.input';
import { ClientService } from '@/client/client.service';
import { IClient } from '@/client/interface/client.interface';

@Controller()
@UseGuards(JwtAuthGuard)
export class ClientController {
  constructor(private readonly clientService: ClientService) {}

  @Get('/api/v1/clients')
  @HttpCode(200)
  async clients(@Body() input: ClientsInput): Promise<IClient[]> {
    return this.clientService.getClients(input.limit, input.name);
  }

  @Get('/api/v1/clients/:id')
  @HttpCode(200)
  async client(@Param('id') id: string): Promise<IClient> {
    if (!id) {
      throw new BadRequestException('Required params: id');
    }
    return this.clientService.getClient(id);
  }

  @Get('/api/v1/clients/:id/policies')
  @HttpCode(200)
  async clientPolicies(@Param('id') id: string): Promise<IPolicy[]> {
    if (!id) {
      throw new BadRequestException('Required params: id');
    }
    return this.clientService.getClientPolicies(id);
  }
}
