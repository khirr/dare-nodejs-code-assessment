import { Injectable, NotFoundException } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { AuthRequestService } from '@/auth/service/auth-request.service';
import { IClientFromServer } from '@/client/interface/client-from-server.interface';
import { IClient } from '@/client/interface/client.interface';
import { PolicyService } from '@/policy/policy.service';
import { IPolicy } from '@/policy/interface/policy.interface';

@Injectable()
export class ClientService {
  constructor(
    private readonly configService: ConfigService,
    private readonly authRequestService: AuthRequestService,
    private readonly policyService: PolicyService,
  ) {}

  private async findClientsFromServer(): Promise<IClientFromServer[]> {
    const url = `${this.configService.get<string>('api.baseUrl')}/clients`;
    return (await this.authRequestService.get(url, {})) as IClientFromServer[];
  }

  private async findClients(): Promise<IClient[]> {
    const policies = await this.policyService.findPoliciesFromServer();
    const serverClients = await this.findClientsFromServer();
    return serverClients.map((serverClient) => ({
      id: serverClient.id,
      name: serverClient.name,
      email: serverClient.email,
      role: serverClient.role,
      policies: this.policyService.getPoliciesByClientId(
        policies,
        serverClient.id,
      ),
    }));
  }

  async getClients(limit = 10, name: string = undefined): Promise<IClient[]> {
    let clients = await this.findClients();
    if (name) {
      clients = clients.filter((client) => client.name.includes(name));
    }
    return clients.slice(0, limit);
  }

  async getClient(clientId: string): Promise<IClient> {
    const clients = await this.findClients();
    const client = clients.find((item) => item.id === clientId);
    if (!client) {
      throw new NotFoundException();
    }
    return client;
  }

  async getClientPolicies(clientId: string): Promise<IPolicy[]> {
    const clients = await this.findClients();
    const client = clients.find((item) => item.id === clientId);
    if (!client) {
      throw new NotFoundException();
    }
    return client.policies;
  }
}
