import { Module } from '@nestjs/common';
import { AuthModule } from '@/auth/auth.module';
import { ClientController } from '@/client/client.controller';
import { ClientService } from '@/client/client.service';
import { PolicyModule } from '@/policy/policy.module';

@Module({
  controllers: [ClientController],
  providers: [ClientService],
  imports: [AuthModule, PolicyModule],
})
export class ClientModule {}
