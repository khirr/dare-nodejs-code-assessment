import { IPolicy } from '@/policy/interface/policy.interface';

export interface IClient {
  id: string;
  name: string;
  email: string;
  role: string;
  policies: IPolicy[];
}
