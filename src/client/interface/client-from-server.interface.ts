export interface IClientFromServer {
  id: string;
  name: string;
  email: string;
  role: string;
}
