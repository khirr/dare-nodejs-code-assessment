FROM node:16.3.0-alpine3.13

# Install dependencies backend
COPY ./package*.json /app/
WORKDIR /app
RUN npm install

# Copy files
COPY . /app

# Build backend
RUN npm run build

# Run
CMD ["npm", "run", "start:prod"]

EXPOSE 3000