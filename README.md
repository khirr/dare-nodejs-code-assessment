### Setup
The .env file is needed, copy .env.example to .env and fill the environment vars.

### Run
Before run the app you need execute: npm install
You can run the project on dev mode using: npm run start:dev

### Run on docker-compose
You can test using docker-compose executing (it will run on 3000 port): docker-compose up 

### Testing
Run end to end testing: npm run test:e2e
